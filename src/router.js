const Router = require('koa-router')
const passport = require('koa-passport')
const GitHubStrategy = require('passport-github').Strategy

const {
  GITHUB_CLIENT_ID,
  GITHUB_CLIENT_SECRET,
  GITHUB_CALLBACK_URL
} = process.env

const verify = (accessToken, refreshToken, profile, cb) =>
  cb({ accessToken, refreshToken, profile })

passport.serializeUser(function(user, done) {
  done(null, user)
})

passport.deserializeUser(function(user, done) {
  done(null, user)
})

passport.use(
  new GitHubStrategy(
    {
      clientID: GITHUB_CLIENT_ID,
      clientSecret: GITHUB_CLIENT_SECRET,
      callbackURL: `${GITHUB_CALLBACK_URL}/auth/github/callback`
    },
    verify
  )
)

module.exports = async function createRouter() {
  const router = Router()

  router.get('/', async ctx => {
    ctx.body = 'Home'
  })

  // oauth
  router.get('/auth/github', passport.authenticate('github'))

  router.get('/auth/github/callback', async ctx =>
    passport.authenticate('github', async response => {
      const { accessToken, refreshToken, profile } = await response
      await ctx.login(profile)
      ctx.body = `Welcome, ${profile.displayName}`
    })(ctx)
  )

  return router
}
